# -*- coding: utf-8 -*-
from trytond.pool import Pool
from .z_lister_anexo import *

def register():
    Pool.register(
        ReportAnexo,
        module='z_lister_anexo', type_='report')
